# Java PlayGround

[![Build Status](https://travis-ci.org/BrianLusina/JvmPlayground.svg?branch=master)](https://travis-ci.org/BrianLusina/JvmPlayground)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/9e25850b02e74288a2681d34de164c47)](https://www.codacy.com/app/BrianLusina/JavaPlayground?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=BrianLusina/JavaPlayground&amp;utm_campaign=Badge_Grade)
[![CircleCI](https://circleci.com/gh/BrianLusina/JavaPlayground.svg?style=svg)](https://circleci.com/gh/BrianLusina/JavaPlayground)

Java PlayGround has a series of small programs and snippets in Java.

To run the tests on the overall project

``` sh
$ gradle test
```
or preferrably use the gradle wrapper

```sh
$ ./gradlew test
```

Enjoy!
