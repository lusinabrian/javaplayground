package com.javapp.stringswords.anagram;


import org.junit.Ignore;
import org.junit.Test;

/**
 * AnagramDetection Tester.
 *
 * @author <Brian Lusina>
 * @version 1.0
 * @since <pre>12/23/2016</pre>
 */
//todo: configure these tests
@Ignore
public class AnagramDetectionTest {

  @Test
  public void shouldDetectNoChangeInCharacters() {
    //assertEquals(anagramDetection('abc', 'abc'), 1);
  }
/*
  it('should detect no change in the characters', function () {
  });

  it('should detect anagrams of itself', function () {
    expect(anagramDetection('aab', 'baa')).to.equal(1);
  });

  it('should detect child anagrams', function () {
    expect(anagramDetection('AbrAcadAbRa', 'cAda')).to.equal(2);
    expect(anagramDetection('AdnBndAndBdaBn', 'dAn')).to.equal(4);
  });

  it('should not fail with a larger child than parent string', function () {
    expect(anagramDetection('test', 'testing')).to.equal(0);
  });
*/


}
