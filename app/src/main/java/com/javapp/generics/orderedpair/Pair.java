package com.javapp.generics.orderedpair;

/**
 * java.generics.orderedpair
 * Created by lusinabrian on 25/12/16.
 * Description:
 */
public interface Pair<K, V> {

  public K getKey();

  public V getValue();
}
