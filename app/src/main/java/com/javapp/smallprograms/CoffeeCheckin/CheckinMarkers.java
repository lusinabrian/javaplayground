package com.javapp.smallprograms.CoffeeCheckin;

enum CheckinMarkers {
  LATE, NEXT, OTHER, DONE,
  MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY,
  BUYS_COFFEE
}
