package com.javapp.enums;

/**
 * com.javapp.enums
 * Created by lusinabrian on 03/03/17.
 * Description: Days of the week
 */
public enum DaysOfWeek {
  MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
}
