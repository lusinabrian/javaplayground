package com.javapp.objects.Bicycle;

/**
 * java.objects.Bicycle
 * Created by lusinabrian on 02/11/16.
 * Description:
 */
class MountainBike extends Bicycle {

  MountainBike(int currentGear, int currentSpeed, int currentPedal, int yearMade, String name,
      String model) {
    super(currentGear, currentSpeed, currentPedal, yearMade, name, model);
  }

}
