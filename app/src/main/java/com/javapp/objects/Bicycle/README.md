Basic package demonstrating classes, inheritance and interfaces in Java using a bicycle object

`Bicycle` class is the superclass that defines all behaviours and states of its subclasses.
`BicycleInterface` is an interface that defines all the interactions with the `Bicycle` states.
`MountainBike` is a subclass of `Bicycle` inherits all its methods including interface interaction
`BicycleDemo` displays the inheritance

                            Bicycle implments BicycleInterface
                                |
                            MountainBike

MountainBike becomes the subclass of Bicycle and therefore the code is modularized and easier to maintain and interact with.
