package com.javapp.objects.AbstractionInterfaces.MotorVehicles;

/**
 * java.objects.AbstractionInterfaces.MotorVehicles
 * Created by lusinabrian on 28/10/16.
 * Description:
 */
interface VehicleInterface {

  /**
   * Does this car have a stero
   */
  void hasStereo(boolean hasStereo);

  /**
   * Color of the body of the car
   */
  String bodyColor(String color);


}
