package com.javapp.algorithms.triangle;

public enum TriangleKind {
  EQUILATERAL,
  ISOSCELES,
  SCALENE
}
